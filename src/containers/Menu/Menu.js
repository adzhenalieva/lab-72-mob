import React from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity, Modal, ScrollView} from 'react-native';
import {connect} from "react-redux";
import {fetchDishes} from "../../store/actions/dishAction";
import {clearCart, closeModal, addPosition, checkout,} from "../../store/actions/orderAction";

import CartList from "../../components/CartList/CartList";

const styles = StyleSheet.create({
    dish: {
        flex: 2,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'black',
        marginVertical: 10,
        paddingHorizontal: 25,
        paddingVertical: 10

    },
    menu: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        marginVertical: 25
    },
    checkout1: {
        borderWidth: 1,
        borderColor: 'black',
        padding: 25,
        backgroundColor: '#FFA500',
        marginTop: 10

    },
    price: {
        marginTop: 10
    }
});


class Menu extends React.Component {

    componentDidMount() {
        this.props.fetchDishes();
    }


    clickHandler = key => {
        const position = {
            name: this.props.dishes[key].dishName,
            price: this.props.dishes[key].price,
        };
        this.props.addPosition(position)
    };

    render() {
        let dishes = null;
        if (this.props.dishes) {
            dishes = this.props.dishes.map((dish, index) => (
                <TouchableOpacity style={styles.dish} key={dish.id} onPress={() => this.clickHandler(index)}>
                    <Image style={{width: 50, height: 50, marginHorizontal: 10}} source={{uri: dish.image}}/>
                    <Text>{dish.dishName}   </Text>
                    <Text>  {dish.price} KGS</Text>
                </TouchableOpacity>
            ))
        }
        return (

                <View style={styles.menu}>
                    <Text style={{fontWeight: 'bold', textAlign: 'center', fontSize: 20}}>Pizza House</Text>
                    <ScrollView>
                    {dishes}
                    </ScrollView>
                    <View>
                        <Text style={styles.price}>Order total: {this.props.totalPrice} KGS</Text>
                        <TouchableOpacity style={styles.checkout1}
                                          onPress={this.props.checkout}><Text>Checkout</Text></TouchableOpacity>
                    </View>
                    <Modal visible={this.props.purchasing}
                           onRequestClose={() => {
                               console.log('Modal closed');
                           }}>
                        <CartList
                            close={this.props.closeModal}
                            clearCart={this.props.clearCart}
                        />
                    </Modal>
                </View>



        );
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dr.dishes,
        totalPrice: state.or.totalPrice,
        purchasing: state.or.purchasing
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDishes: () => dispatch(fetchDishes()),
        addPosition: position => dispatch(addPosition(position)),
        checkout: () => dispatch(checkout()),
        closeModal: () => dispatch(closeModal()),
        clearCart: () => dispatch(clearCart())
    };

};


export default connect(mapStateToProps, mapDispatchToProps)(Menu);
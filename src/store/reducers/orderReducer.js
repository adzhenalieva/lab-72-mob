import {
    ADD_POSITION,
    CHECKOUT,
    CLEAR_CART,
    CLOSE_MODAL,
    ORDER_FAILURE,
    ORDER_REQUEST,
    ORDER_SUCCESS,
    REMOVE_POSITION
} from "../actions/actionTypes";

const initialState = {
    cart: [],
    loading: false,
    purchasing: false,
    totalPrice: 150,
    error: null
};

const dishesCombineReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POSITION:
            let order;

            if(state.cart[action.position.name]) {
                order = {...action.position, amount: state.cart[action.position.name].amount + 1}
            } else {
                order = {...action.position, amount: 1};
            }
            const orders = {...state.cart, [action.position.name]: order};
            return {
                ...state, cart: orders,
                totalPrice: state.totalPrice + parseInt(action.position.price),
            };
        case CHECKOUT:
            return {
                ...state,
                purchasing: true
            };
        case REMOVE_POSITION:
            let cart = {...state.cart};
            let newAmount = cart[action.position].amount - 1;
            cart[action.position].amount = newAmount;
            return {
                ...state, cart: cart,
                totalPrice: state.totalPrice - cart[action.position].price
            };
        case CLOSE_MODAL:
            return {
                ...state,
                purchasing: false
            };
        case CLEAR_CART:
            return {
                ...state,
                cart: [],
                totalPrice: 150
            };
        case ORDER_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case ORDER_SUCCESS:
            return {
                ...state,
                loading: false,
                purchasing: false,
            };
        case ORDER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        default:
            return state;
    }
};
export default dishesCombineReducer;
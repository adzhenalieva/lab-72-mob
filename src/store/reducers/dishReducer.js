import {
    DISH_FAILURE,
    DISH_REQUEST,
    DISH_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    dishes: [],
    loading: false,
    error: null
};

const dishesCombineReducer = (state = initialState, action) => {
    switch (action.type) {
        case DISH_SUCCESS:
            return {
                ...state,
                dishes: action.response, loading: true
            };
        case DISH_REQUEST:
            return {
                ...state, loading: false
            };
        case DISH_FAILURE:
            return {
                ...state,
                loading: true,
                error: action.error
            };
        default:
            return state;
    }
};
export default dishesCombineReducer;



export const DISH_REQUEST = 'DISH_REQUEST';
export const DISH_SUCCESS = 'DISH_SUCCESS';
export const DISH_FAILURE = 'DISH_FAILURE';

export const ADD_POSITION = 'ADD_POSITION';
export const REMOVE_POSITION = 'REMOVE_POSITION';
export const CLEAR_CART = 'CLEAR_CART';
export const CHECKOUT = 'CHECKOUT';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILURE = 'ORDER_FAILURE';

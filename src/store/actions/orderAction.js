import axios from "../../../axios-dishes";
import {
    ADD_POSITION, CHECKOUT,
    CLEAR_CART,
    CLOSE_MODAL,
    ORDER_FAILURE,
    ORDER_REQUEST,
    ORDER_SUCCESS,
    REMOVE_POSITION
} from "./actionTypes";

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = () => ({type: ORDER_SUCCESS});
export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const createOrder = (order) => {
    return dispatch => {
        dispatch(orderRequest());
        axios.post('/dishesOrders.json', order).then(
            () => {
                dispatch(orderSuccess());
            },
            error => dispatch(orderFailure(error))
        );
    }
};

export const removePosition = position => ({type: REMOVE_POSITION, position});

export const closeModal = () => ({type: CLOSE_MODAL});

export const clearCart = () => ({type: CLEAR_CART});

export const addPosition = position => ({type: ADD_POSITION, position});

export const checkout = () => ({type: CHECKOUT});
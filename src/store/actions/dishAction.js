import axios from '../../../axios-dishes';
import {DISH_FAILURE, DISH_REQUEST, DISH_SUCCESS} from "./actionTypes";

export const dishRequest = () => ({type: DISH_REQUEST});
export const dishSuccess = response => ({type: DISH_SUCCESS, response});
export const dishFailure = error => ({type: DISH_FAILURE, error});

export const fetchDishes = () => {
    return dispatch => {
        dispatch(dishRequest());
        axios.get('/dishes.json').then(response => {
            const dishes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            dispatch(dishSuccess(dishes));
        }, error => {
            dispatch(dishFailure(error));
        });
    }
};

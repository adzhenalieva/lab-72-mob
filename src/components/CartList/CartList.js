import React from "react";
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {removePosition, closeModal, createOrder} from "../../store/actions/orderAction";
import {connect} from "react-redux";

const styles = StyleSheet.create({
    cartList: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'black',
        marginVertical: 10,
        padding: 15,
        flexDirection: 'row'
    },
    button: {
        backgroundColor: '#FFA500',
        borderWidth: 1,
        borderColor: 'black',
        padding: 10,
        marginVertical: 10,
    },
    order: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        margin: 25
    },
    buttonDel: {
        backgroundColor: '#FFA500',
        borderWidth: 1,
        borderColor: 'black',
        paddingHorizontal: 5,
        marginLeft: 10

    },
});

class CartList extends React.Component {

    orderHandler = event => {
        event.preventDefault();
        let positions = {};
        Object.keys(this.props.cart).forEach(dish => {
            positions[dish] = this.props.cart[dish].amount
        });
        const orderData = {
            positions

        };

        this.props.createOrder(orderData);
        this.props.close();
        this.props.clearCart();
    };

    render() {
        return (
            <View style={styles.order}>
                {Object.keys(this.props.cart).map((ing, key) => {
                    return (
                        this.props.cart[ing].amount < 1 ? null : <View style={styles.cartList} key={key}>
                            <Text>{this.props.cart[ing].name} </Text>
                            <Text>{this.props.cart[ing].amount} x </Text>
                            <Text style={{fontWeight: 'bold'}}>  {this.props.cart[ing].price} KGS</Text>
                            <TouchableOpacity style={styles.buttonDel} onPress={() => this.props.remove(ing)}><Text>X</Text></TouchableOpacity>
                        </View>
                    )
                })}
                <Text>Total price: {this.props.totalPrice} KGS</Text>
               <TouchableOpacity  style={styles.button} onPress={this.orderHandler}><Text>Order</Text></TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={this.props.closeModal}><Text>Cancel</Text></TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        cart: state.or.cart,
        totalPrice: state.or.totalPrice,
        purchasing: state.or.purchasing,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        remove: (name) => dispatch(removePosition(name)),
        closeModal: () => dispatch(closeModal()),
        createOrder: (orderData) => dispatch(createOrder(orderData)),



    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartList);
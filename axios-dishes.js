import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://my-blog-elzaais.firebaseio.com/'
});

export default instance;
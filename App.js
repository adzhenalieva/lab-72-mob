import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {Provider} from 'react-redux';
import dishReducer from './src/store/reducers/dishReducer';
import orderReducer from './src/store/reducers/orderReducer';
import Menu from "./src/containers/Menu/Menu";

const rootReducer = combineReducers({
    dr: dishReducer,
    or: orderReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunkMiddleware))
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <Menu/>
                </View>
            </Provider>
        );
    }
}


